FROM alpine 
#FROM debian:jessie

LABEL Maintainer "Shijie Yao syao@lbl.gov"

# create download and move into it
WORKDIR /workdir

# install the needed programs: alpine-sdk (~ ubuntu's build-essential)
RUN apk add --no-cache wget unzip alpine-sdk && \
   wget http://last.cbrc.jp/last-849.zip && \
   unzip last-849.zip; rm last-849.zip  && \
   cd last-849; make install; cd ..; rm -rf last-849 &&\
   apk del wget unzip 

