# LAST aligner docker image #

LAST last-849.zip is installed in this image.

LAST executables are in program search path. 

To run:

docker run --rm last:latest --volume:LOCAL_ABS_PATH:IMG_ABS_PATH lastdb -cR01 humdb /IMG_ABS_PATH/humanMito.fa
